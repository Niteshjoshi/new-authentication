Rails.application.routes.draw do

 root to: 'users#new'
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  get 'logout' => 'sessions#destroy'

  get 'signup' => 'users#new'
  post 'users' => 'users#create'
  get  'log' => 'welcome#log'
  get 'users/:id' => "users#show", as: 'user_show'
  get 'user_edit/:id' => 'users#edit' , as: 'user_edit'
  patch 'user_update/:id'=> 'users#update', as: 'user_update'
end
