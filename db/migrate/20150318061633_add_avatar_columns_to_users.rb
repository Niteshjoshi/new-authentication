class AddAvatarColumnsToUsers < ActiveRecord::Migration
  # def change
  # 	 add_attachment :users, :avatar
  # end

  def self.up
    add_attachment :users, :avatar
  end

  def self.down
    remove_attachment :users, :avatar
  end
end
