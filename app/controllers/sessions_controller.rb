class SessionsController < ApplicationController
  def new
  end

  def create
  	user = User.find_by_email(params[:email])
  	 if user.present? && user.authenticate(params[:password])
  	 	session[:user_id] = user.id
     # render 'log', :notice =>"Logged IN!!"
     redirect_to log_path
    else
      flash[:notice]= 'invalid credentials'
    	render 'new'
    end
  end

  def destroy
  	session[:user_id] = nil
    redirect_to login_path
  end
  
  
end
