lass UsersController < ApplicationController
  def new
    @user=User.new
  end

  def create
  	@user = User.new(user_params)

    if @user.save
      session[:user_id] = @user.id
      redirect_to root_path ,:notice =>"Signed Up!!"
    else
    #  flash[:error]='user creation failed'
      render 'new'
    end
  end

  def edit
    # current_user = User.find(session[:user_id]) if session[:user_id]
  end
   
  def update
  # current_user = User.find(session[:user_id]) if session[:user_id]

  if current_user.update(user_params)
    redirect_to user_show_path(current_user.id)
  else
    render 'edit'
  end
  end

  def show

  end


private
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :avatar)
  end
end

